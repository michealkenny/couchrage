#!/bin/bash

#                   #
# INSTALL FUNCTIONS #
#                   #

function install_requirements {
    #Add additional repositories and install dependencies
    apt-get update
    apt-get -y install git-core python python-cheetah
}

function install_couchpotato {
    #Install CouchPotato
    git clone --depth=1 git://github.com/RuudBurger/CouchPotatoServer.git /opt/couchpotato
    cp /opt/couchpotato/init/ubuntu /etc/init.d/couchpotato
    cp configs/couchpotato-default /etc/default/couchpotato
}

function install_sickrage {
    #Install SickRage
    git clone --depth=1 git://github.com/SickRage/SickRage.git /opt/sickrage
    cp configs/sickrage-init /etc/init.d/sickrage
    cp configs/sickrage-default /etc/default/sickrage
}

function install_sabnzbd {
    #Install SABnzbd
    apt-get -y install sabnzbdplus
    cp configs/sabnzbd-default /etc/default/sabnzbdplus
}

function install_transmission {
    #Install Transmission
    apt-get -y install transmission-cli transmission-common transmission-daemon
    service transmission-daemon stop
    cp configs/transmission-settings /etc/transmission-daemon/settings.json
    service transmission-daemon stop
}

function chmod_scripts {
    #Make scripts executible and update boot sequence
    chmod +x /etc/init.d/couchpotato /etc/init.d/sickrage
    chmod +x /etc/default/couchpotato /etc/default/sickrage
    update-rc.d sickrage defaults
    update-rc.d couchpotato defaults
}

#                   #
# GENERAL FUNCTIONS #
#                   #

function manage_services {
    exit_array=()

    #Restart services
    service couchpotato $1 > /dev/null 2>&1; exit_array+=("$?")
    service sickrage $1 > /dev/null 2>&1; exit_array+=("$?")
    service sabnzbdplus $1 > /dev/null 2>&1; exit_array+=("$?")
    service transmission-daemon $1 > /dev/null 2>&1; exit_array+=("$?")

    #Check if any of the above commands exited with a non-zero.
    for exit_code in "${exit_array[@]}"; do
        if [ $exit_code -ne 0 ]; then
            echo "ERROR 136: Error managing 1 or more of the services, is CouchRage installed?"
            exit 136
        fi
    done
}

function general_checks {
    #Check if user is root and there are arguments given.
    if [ "$EUID" -ne 0 ]; then
        echo "ERROR 130: CouchRage needs to be run with root privileges, do so with sudo bash couchrage.sh"
        exit 130

    elif [ "$#" -eq 0 ]; then
        echo -e "ERROR 134: Command line arguments required.\nUsage:\tsudo bash couchrage.sh\t [-i --install]\n\t\t\t\t [-u --uninstall]\n\t\t\t\t [-h --help]\n\t\t\t\t [-p --pull-latest]\n\t\t\t\t [-m --manage start|stop|restart]"
        exit 134

    fi
}


function install_checks {
    #Function to perform a number of checks before continuing with install.

    #Check if kernal version includes "Ubuntu".
    if [[ $(uname -v) != *Ubuntu* ]]; then
        read -p "ERROR 131: CouchRage was designed for Ubuntu and Ubuntu Server, do you wish to continue? y/[N]: " response

        case $response in
            [yY]* )
                ;;
            * )
                exit 131
                ;;
        esac
    fi

    #Check if required ports are available.
    case $(netstat -ln) in
        *5050* )
            echo "ERROR 132: CouchRage needs port 5050, but it is currently in use."
            exit 132
            ;;
        *8081* )
            echo "ERROR 132: CouchRage needs port 8081, but it is currently in use."
            exit 132
            ;;
        *9091* )
            echo "ERROR 132: CouchRage needs port 9091, but it is currently in use."
            exit 132
            ;;
        *9092* )
            echo "ERROR 132: CouchRage needs port 9092, but it is currently in use."
            exit 132
            ;;
    esac

    #Check if install folders already exist, and exit with 133 if so.
    if [ -d /opt/couchpotato ]; then
        echo "ERROR 133: /opt/couchpotato directory already exists, exiting."
        exit 133

    elif [ -d /opt/sickrage/ ]; then
        echo "ERROR 133: /opt/sickrage directory already exists, exiting."
        exit 133

    elif [ -d /etc/transmission-daemon/ ]; then
        echo "ERROR 133: /etc/transmission-daemon/ directory already exists, exiting."
        exit 133

    fi
}

#           #
# MAIN CASE #
#           #

general_checks $1

case "$1" in
    -i|--install )
        echo -e "\e[31m"; cat header

        echo -e "\n\e[32m[CouchRage] >> Performing prerequisite checks..\e[39m"
        install_checks

        echo -e "\n\e[32m[CouchRage] >> Updating repos and installing requirements..\e[39m"
        install_requirements
        
        echo -e "\n\e[32m[CouchRage] >> Installing CouchPotato..\e[39m"
        install_couchpotato
        
        echo -e "\n\e[32m[CouchRage] >> Installing SickRage..\e[39m"
        install_sickrage
        
        echo -e "\n\e[32m[CouchRage] >> Installing SABnzbd..\e[39m"
        install_sabnzbd
        
        echo -e "\n\e[32m[CouchRage] >> Installing Transmission..\e[39m"
        install_transmission
        
        echo -e "\n\e[32m[CouchRage] >> Finalizing install..\e[39m"
        chmod_scripts
        manage_services restart
        
        echo -e "\n\e[32m[CouchRage] >> Install successful! \e[39m"
        echo -e "\nGeneral Information\n===================\n\e[4mService\t\tPort\e[0m\nCouchPotato\t5050\nSickRage\t8081\nSABnzbd\t\t9092\nTransmission\t9091\n\n"
        exit 0
        ;;
    -u|--uninstall )
        echo -e "\e[31m"; cat header

        echo -e "\n\e[32m[CouchRage] >> Stopping services..\e[39m"
        manage_services stop

        echo -e "\n\e[32m[CouchRage] >> Removing CouchPotato..\e[39m"
        rm -rf /opt/couchpotato
        rm -rf /etc/default/couchpotato
        rm -rf /etc/init.d/couchpotato

        echo -e "\n\e[32m[CouchRage] >> Removing SickRage..\e[39m"
        rm -rf /opt/sickrage
        rm -rf /etc/default/sickrage
        rm -rf /etc/init.d/sickrage
        
        echo -e "\n\e[32m[CouchRage] >> Removing SABnzbd..\e[39m"
        apt-get -y purge sabnzbdplus > /dev/null 2>&1
        
        echo -e "\n\e[32m[CouchRage] >> Removing Transmission..\e[39m"
        apt-get -y purge transmission-daemon > /dev/null 2>&1
        
        echo -e "\n\e[32m[CouchRage] >> Uninstall Successful! \e[39m"
        exit 0
        ;;
    -p|--pull-latest )
        echo -e "\e[31m"; cat header
        
        echo -e "\n\e[32m[CouchRage] >> Stopping services..\e[39m"
        manage_services stop

        echo -e "\n\e[32m[CouchRage] >> Updating CouchPotato..\e[39m"
        cd /opt/couchpotato && git pull
        
        echo -e "\n\e[32m[CouchRage] >> Updating SickRage..\e[39m"
        cd /opt/sickrage && git pull
        
        echo -e "\n\e[32m[CouchRage] >> Stating services..\e[39m"
        manage_services start
        
        echo -e "\n\e[32m[CouchRage] >> Update Successful! \e[39m"
        exit 0
        ;;
    -h|--help )
        echo -e "Usage:\tsudo bash couchrage.sh\t [-i --install]\n\t\t\t\t [-u --uninstall]\n\t\t\t\t [-h --help]\n\t\t\t\t [-p --pull-latest]\n\t\t\t\t [-m --manage start|stop|restart]"
        echo -e "\nGeneral Information\n===================\n\e[4mService\t\tPort\e[0m\nCouchPotato\t5050\nSickRage\t8081\nSABnzbd\t\t9092\nTransmission\t9091\n\n"
        exit 0
        ;;
    -m|--manage )
        manage_services $2
        exit 0
        ;;
    * )
        echo -e "ERROR 135: Improper usage.\nUsage:\tsudo bash couchrage.sh\t [-i --install]\n\t\t\t\t [-u --uninstall]\n\t\t\t\t [-h --help]\n\t\t\t\t [-p --pull-latest]\n\t\t\t\t [-m --manage start|stop|restart]"
        exit 135
        ;;
esac
